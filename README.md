# [Gradle Catalogs](https://docs.gradle.org/current/userguide/platforms.html#sub:using-platform-to-control-transitive-deps)

---

> **Central declaration of dependencies**. </br>
> A version catalog is a list of dependencies, represented as dependency coordinates, that a user can pick from when declaring dependencies in a build script.

## Spring Boot Catalog
How to use custom spring boot catalog dependencies.
```
settings.gradle

dependencyResolutionManagement {
    repositories {
        maven {
            url 'https://gitlab.com/api/v4/projects/43975196/packages/maven'
        }
    }

    versionCatalogs {
        springBootCatalog {
            from("com.konigsberger.www.gradle-catalogs:spring-catalog:0.0.1")
        }
    }
}
```
